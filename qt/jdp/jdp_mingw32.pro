#-------------------------------------------------
#
# Project created by QtCreator 2019-02-15T13:46:51
#
#-------------------------------------------------

QT       -= core gui

TARGET = jdp
TEMPLATE = lib

DEFINES += JDP_LIBRARY

#CONFIG += c++14
#QMAKE_CXXFLAGS += -std=c++14
CONFIG += c++14

INCLUDEPATH += ../../inc
INCLUDEPATH += ../../thirdparty/rapidjson/inc
INCLUDEPATH += ../../thirdparty/MurmurHash3/inc

SOURCES += \
    ../../src/jdp.cpp\
    ../../thirdparty/MurmurHash3/src/MurmurHash3.cpp

HEADERS += ../../inc/jdp.h\
        ../../inc/jdp_global.h\
        ../../inc/cwraptypes.h\
        ../../inc/jsonkeyhashes.h\
        ../../thirdparty/MurmurHash3/inc/MurmurHash3.h

# копирование конечных файлов в корень каталога сборки
unix {
QMAKE_POST_LINK=cp -P ./libjdp* ../test_app
}

win32 {
    CONFIG(debug, debug|release) {
        QMAKE_POST_LINK=echo f | xcopy /y  .\debug\jdp.dll ..\jdp.dll
    }
    CONFIG(release, debug|release) {
        QMAKE_POST_LINK=echo f | xcopy /y  .\release\jdp.dll ..\jdp.dll
    }
}
