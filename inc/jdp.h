﻿#ifndef JDP_H
#define JDP_H

#include "jdp_global.h"
#include <cstddef>
#include <cstdint>
#include "cwraptypes.h"

namespace Error
{
    enum Code
    {
	NoError = 0,

	Malloc_function_not_assigned,
	Malloc_failed,
	DocumentIsNull,
	DocumentIsNotObject,
	JsonTopLayoutMismatch,
	
    	MissingVersion,
	VersionIsNotString,
	VersionMismatch,
	
    	MissingUnitsObject,
	UnitsIsNotObject,
	
    	MissingDataObject,
	DataIsNotObject,
	
    	FileOpenError,
	DocumentParseError,
	TypeIdIsNotMapped,
	XpathPointsToNothing,
	XpathValueIsNotObject,
	XpathIsNull,
	XPathMissingReshetka,
	MissingBitPosInfoForOptField,
	JsonFieldTypeMismatch,
	MissingOptionalValuesTable,
	AtUnreachable,
	MissingRequiredField,
	InappropriatePresncType,

    	ArrayDbl3x3_IsNotArray,
	ArrayDbl3x3_Size_is_not_3,

    	ArrayDbl1x3_IsNotArray,
	ArrayDbl1x3_Size_is_not_3,
	ArrayDbl1x3_ElementIsNotNumber,

	ArrayChar1x3_IsNotString,
	ArrayChar1x3_Size_gt_3,

	ETblDbl_IsNotArray,
	ETblDbl_IsEmpty,
	ETblDbl_SubArrayIsNotArray,
	ETblDbl_SubArrayIsEmpty,
	ETblDbl_LeafElementIsNotNumber,
	ETblDbl_SubArraySizeMismatch,
	ETblDbl_AllSubArraysAreNullObjects,

	ER3ADbl_IsNotArray,
	ER3ADbl_IsEmpty,
	ER3ADbl_SubArray_IsNotArray,
	ER3ADbl_SubArray_IsEmpty,
	ER3ADbl_SubSubArray_IsNotArray,
	ER3ADbl_SubSubArray_IsEmpty,
	ER3ADbl_LeafElementIsNotNumber,
	ER3ADbl_SubArraySizeMismatch,
	ER3ADbl_SubSubArraySizeMismatch,
	ER3ADbl_AllSubArraysAreNullObjects,
	ER3ADbl_AllSubSubArraysAreNullObjects,

	mean_profile_drag_IsNotObject,
	mean_profile_drag_Missing_CombiTableDbl_Field,
	mean_profile_drag_CombiTableDbl_IsNull,
	mean_profile_drag_CombiTableDbl_IsNotObject,

	EArrayDbl_InappropriateElementType,
	process_number_unexpected_number_type,

	init_parser_from_file_null_this,
	init_parser_from_string_null_this,
	set_alloc_null_this,
	fini_parser_null_this,
	parse_object_null_this,
	parse_object_null_xpath,
	parse_object_null_output_buffer,
	get_error_string_null_this,
	get_error_string_null_strptr,
	wrong_error_id,
	null_this,
    };
};

namespace JsonType
{
    enum Enum
    {
	Null = 0,
	False = 1,
	True = 2,
	Object = 3,
	Array = 4,
	String = 5,
	Number = 8,
	Uint8 = 10,
	Uint16 = 11,
	Uint32 = 12,
	UInt64 = 13,
	Float = 14,
	Double = 15
    };

};

namespace TypeIds
{
    enum Enum
    {
	Unknown = 0,
	param_one_rotor_helicopter,
	param_fuselage,
	units_Fuselage,
	param_V_tail,
	units_VTail,
	param_H_tail,
	UnitsHTail,
	param_rotor_simple,
	UnitsRotorSimple,
	param_cet_1m1t_rotor,
	param_swashplate,
	UnitsSwashplate,
	param_hub_simple,
	param_hinge_general,
	UnitsHingeGeneral,
	param_transmission_1m1t_rotor,
	UnitsTransmission1M1T,
	param_enginewithregulator,
	param_blade_simple,
	UnitsBladeSimple,
	VectorCombiTableDbl,
	T_factor,
	UnitsTwist,
	T_position,
	UnitsPosition,
	Combi3TableDbl,
	CombiTableDbl,
	EPoly2Dbl,
	UnitsInertia,
	UnitsCombi3Table,
	UnitsCombiTable,
    };
}

#ifdef __cplusplus
extern "C" {
#endif

    typedef void* (CDECL *fn_malloc_t)(uint32_t size);

    // init parser. creates parser object and parse json to DOM document (rapidjson terminology)
    JDPSHARED_EXPORT Error::Code init_parser_from_file(const char* fname, void**_this);
    JDPSHARED_EXPORT Error::Code init_parser_from_string(const char* json_text, void**_this);

    // alloc function must return pointer to allocated mem block or nullptr on fail
    JDPSHARED_EXPORT Error::Code set_alloc(void* _this, fn_malloc_t fn);

    // finalize parser
    JDPSHARED_EXPORT Error::Code fini_parser(void* _this);

    // deserialize json object, pointed by xpath, to buf. buf must be allocated by library client.
    // type_id - type of the C-object
    // co - pointer to C-object
    JDPSHARED_EXPORT Error::Code parse_object(void* _this, TypeIds::Enum type_id, const char* xpath, void* co);
	
    JDPSHARED_EXPORT Error::Code get_error_string(void* _this, Error::Code err, struct EString* str);

    // данные функции предназначены главным образом для отладочных целей
    JDPSHARED_EXPORT Error::Code test_init_parser_from_string(const char* json_text, void**_this);

    JDPSHARED_EXPORT Error::Code test_parse_string(void* _this, EString* co);
    JDPSHARED_EXPORT Error::Code test_parse_1x3_dbl_array(void* _this, void*co);
    JDPSHARED_EXPORT Error::Code test_parse_3x3_dbl_array(void* _this, void*co);
    JDPSHARED_EXPORT Error::Code test_parse_1x3_char_array(void* _this, void* co);
    JDPSHARED_EXPORT Error::Code test_parse_EArrayDbl(void* _this, EArrayDbl *co);
    JDPSHARED_EXPORT Error::Code test_parse_ETableDbl(void* _this, ETableDbl *co);
    JDPSHARED_EXPORT Error::Code test_parse_ERank3ArrayDbl(void* _this, ERank3ArrayDbl *co);
    JDPSHARED_EXPORT Error::Code test_parse_EPoly2Dbl(void* _this, EPoly2Dbl *co);
    JDPSHARED_EXPORT Error::Code test_parse_UnitsInertia(void* _this, UnitsInertia *co);
    JDPSHARED_EXPORT Error::Code test_parse_UnitsCombiTable(void* _this, UnitsCombiTable *co);
    JDPSHARED_EXPORT Error::Code test_parse_UnitsCombi3Table(void* _this, UnitsCombi3Table *co);
    JDPSHARED_EXPORT Error::Code test_parse_CombiTableDbl(void* _this, CombiTableDbl *co);
    JDPSHARED_EXPORT Error::Code test_parse_Combi3TableDbl(void* _this, Combi3TableDbl *co);
    JDPSHARED_EXPORT Error::Code test_parse_UnitsPosition(void* _this, UnitsPosition *co);
    JDPSHARED_EXPORT Error::Code test_parse_T_position(void* _this, T_position *co);
    JDPSHARED_EXPORT Error::Code test_parse_T_factor(void* _this, T_factor *co);
    JDPSHARED_EXPORT Error::Code test_parse_T_VectorCombiTableDbl(void* _this, T_VectorCombiTableDbl *co);
    JDPSHARED_EXPORT Error::Code test_parse_UnitsTwist(void* _this, UnitsTwist *co);

    JDPSHARED_EXPORT Error::Code test_parse_UnitsBladeSimple(void* _this, UnitsBladeSimple *co);
    JDPSHARED_EXPORT Error::Code test_parse_T_param_blade_simple(void* _this, T_param_blade_simple *co);
    JDPSHARED_EXPORT Error::Code test_parse_T_param_enginewithregulator(void* _this, T_param_enginewithregulator *co);
    JDPSHARED_EXPORT Error::Code test_parse_UnitsTransmission1M1T(void* _this, UnitsTransmission1M1T *co);
    JDPSHARED_EXPORT Error::Code test_parse_T_param_transmission_1m1t_rotor(void* _this, T_param_transmission_1m1t_rotor *co);
    JDPSHARED_EXPORT Error::Code test_parse_UnitsHingeGeneral(void* _this, UnitsHingeGeneral *co);
    JDPSHARED_EXPORT Error::Code test_parse_T_param_hinge_general(void* _this, T_param_hinge_general *co);
    JDPSHARED_EXPORT Error::Code test_parse_T_param_hub_simple(void* _this, T_param_hub_simple *co);
    JDPSHARED_EXPORT Error::Code test_parse_UnitsSwashplate(void* _this, UnitsSwashplate *co);
    JDPSHARED_EXPORT Error::Code test_parse_T_param_swashplate(void* _this, T_param_swashplate *co);
    JDPSHARED_EXPORT Error::Code test_parse_T_param_cet_1m1t_rotor(void* _this, T_param_cet_1m1t_rotor *co);
    JDPSHARED_EXPORT Error::Code test_parse_UnitsRotorSimple(void* _this, UnitsRotorSimple *co);
    JDPSHARED_EXPORT Error::Code test_parse_T_param_rotor_simple(void* _this, T_param_rotor_simple *co);
    JDPSHARED_EXPORT Error::Code test_parse_UnitsHTail(void* _this, UnitsHTail *co);
    JDPSHARED_EXPORT Error::Code test_parse_T_param_H_tail(void* _this, T_param_H_tail *co);
    JDPSHARED_EXPORT Error::Code test_parse_UnitsVTail(void* _this, UnitsVTail *co);
    JDPSHARED_EXPORT Error::Code test_parse_T_param_V_tail(void* _this, T_param_V_tail *co);
    JDPSHARED_EXPORT Error::Code test_parse_UnitsFuselageSimple(void* _this, UnitsFuselageSimple *co);
    JDPSHARED_EXPORT Error::Code test_parse_T_param_fuselage_simple(void* _this, T_param_fuselage_simple *co);
    JDPSHARED_EXPORT Error::Code test_parse_T_param_simple_one_rotor_helicopter(void* _this, T_param_simple_one_rotor_helicopter *co);
    
#ifdef __cplusplus
}
#endif

#endif // JDP_H
